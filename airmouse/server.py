import ipaddress
import os

from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.log import enable_pretty_logging
from tornado.web import Application

from .handlers import PageHandler, SocketHandler


class WebServer(object):
    def __init__(self, host='localhost', port=8765):
        self.host = os.getenv('SERVER_HOST', default=host)
        self.port = os.getenv('SERVER_PORT', default=port)
        self.host_filter = self._build_host_filters()
        self.app_server = None
        self.http_server = None
    
    def _build_host_filters(self):
        host_filters = []
        try:
            ipaddress.ip_address(self.host)
            normalized_host = self.host.replace(".", "\\.")
            host_filters.append(normalized_host)
            if self.host == '127.0.0.1':
                host_filters.append('localhost')
        except ValueError:
            host_filters.append(self.host)
            if self.host == 'localhost':
                host_filters.append('127\\.0\\.0\\.1')
        return '(' + (r"|".join(host_filters)) + ')'

    def start(self):
        enable_pretty_logging()

        # create Tornado application, supply URL routes
        self.app_server = Application(
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            template_path=os.path.join(os.path.dirname(__file__), "templates")
        )

        self.app_server.add_handlers(self.host_filter, [
            (r"/", PageHandler),
            (r"/inputsocket", SocketHandler),
        ])

        # setup HTTP Server
        self.http_server = HTTPServer(self.app_server)
        self.http_server.listen(self.port, address=self.host)
        print(f'Start listening at {self.host}:{self.port}')
    
        # start event loop
        IOLoop.current().start()
