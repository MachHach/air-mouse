function updatePanelSize() {
    panel = document.getElementById("panel");
    width = 100
    height = 100

    screenRatio = screen.height / screen.width;
    pageRatio = document.body.clientHeight / document.body.clientWidth;
    // if client resolution is wider than server
    if(pageRatio < screenRatio) {
        width = width * (pageRatio / screenRatio)
    } else {
        height = height * (screenRatio / pageRatio)
    }
    panel.style.width = width + '%'
    panel.style.height = height + '%'
}

function initResizeHandler() {
    window.addEventListener('resize', function() {
        updatePanelSize()
    }, true);
}

var updater = {
    socket: null,

    start: function() {
        var url = "ws://" + location.host + "/inputsocket";
        updater.socket = new WebSocket(url);
    },
};

var pointHandler = {
    SCLICK: "sclick",
    DCLICK: "dclick",
    RCLICK: "rclick",
    DCLICK_DELAY_MS: 500,
    DEVIATE_MIN_PX: 75,
    lastID: 0,

    currentMetaJob: {},
    jobs: [],

    initCurrentMetaJob: function(event) {
        this.currentMetaJob = {
            id: 0,
            data: {
                t: "",
                w: event.currentTarget.offsetWidth,
                h: event.currentTarget.offsetHeight,
                x: event.pageX - event.currentTarget.offsetLeft,
                y: event.pageY - event.currentTarget.offsetTop,
            }
        };
    },

    start: function() {
        Pressure.set('#panel', {
            start: function(event) {
                pointHandler.initCurrentMetaJob(event);

                // find any pending single-click job with the same position
                pendingSingleClickJobs = pointHandler.jobs.filter(function(job) {
                    return job.data.t === pointHandler.SCLICK
                        && job.data.w === pointHandler.currentMetaJob.data.w
                        && job.data.h === pointHandler.currentMetaJob.data.h
                        && Math.abs(job.data.x - pointHandler.currentMetaJob.data.x) <= pointHandler.DEVIATE_MIN_PX
                        && Math.abs(job.data.y - pointHandler.currentMetaJob.data.y) <= pointHandler.DEVIATE_MIN_PX;
                });
                if(pendingSingleClickJobs.length > 0) {
                    // upgrade to double-click
                    firstMatchingJob = pendingSingleClickJobs[0];
                    firstMatchingJob.data.t = pointHandler.DCLICK;
                    pointHandler.currentMetaJob = firstMatchingJob;
                } else {
                    // create a single-click job for current position
                    pointHandler.currentMetaJob.data.t = pointHandler.SCLICK;
                    pointHandler.currentMetaJob = pointHandler.createJob(pointHandler.currentMetaJob.data);
                }
            },
            end: function() {
                // trigger double-click / right-click job, only if it is related to current meta job
                pendingClickJobs = pointHandler.jobs.filter(function(job) {
                    return job.data.id === pointHandler.currentMetaJob.data.id
                        && (job.data.t === pointHandler.DCLICK || job.data.t === pointHandler.RCLICK);
                });
                if(pendingClickJobs.length > 0) {
                    pointHandler.dispatchJob(pendingClickJobs[0]);
                }

                // fires a timer that trigger single-click job, only if it is related to current meta job and still exists
                if(pointHandler.currentMetaJob.data.t === pointHandler.SCLICK) {
                    window.setTimeout(function(id) {
                        pendingSingleClickJobs = pointHandler.jobs.filter(function(job) {
                            return job.data.id === id
                                && job.data.t === pointHandler.SCLICK;
                        });
                        if(pendingSingleClickJobs.length > 0) {
                            pointHandler.dispatchJob(pendingSingleClickJobs[0]);
                        }
                    }, pointHandler.DCLICK_DELAY_MS, pointHandler.currentMetaJob.data.id);
                }
            },
            startDeepPress: function(event){
                currentMetaTrigger = pointHandler.currentMetaJob.data.t;
                
                if(currentMetaTrigger === pointHandler.DCLICK) {
                    // upon double-click + deep press, upgrade to right-click
                    pendingDoubleClickJobs = pointHandler.jobs.filter(function(job) {
                        return job.data.id === pointHandler.currentMetaJob.data.id
                            && (job.data.t === pointHandler.DCLICK || job.data.t === pointHandler.RCLICK);
                    });
                    if(pendingDoubleClickJobs.length > 0) {
                        firstMatchingJob = pendingDoubleClickJobs[0];
                        firstMatchingJob.data.t = pointHandler.RCLICK;
                        pointHandler.currentMetaJob = firstMatchingJob;
                    }
                } else if(currentMetaTrigger === pointHandler.SCLICK) {
                    // upon single-click + deep press, upgrade to drag mode
                    // TODO
                }
            },
            endDeepPress: function(){},
            change: function(force, event){},
            unsupported: function(){},
        });
    },

    createJob: function(inputData) {
        this.lastID++;

        inputJob = {
            id: this.lastID,
            data: inputData,
        };

        this.jobs.push(inputJob);

        return inputJob;
    },

    dispatchJob: function(job) {
        this.removeJob(job.id);
        updater.socket.send(JSON.stringify(job.data));
    },

    removeJob: function(id) {
        for(var i = 0; i < this.jobs.length; i++) {
            if(this.jobs[i].id === id) {
                this.jobs.splice(i, 1);
                break;
            }
        }
    },
};

window.onload = function() {
    updatePanelSize();
    initResizeHandler();
    
    updater.start();
    pointHandler.start();
};
