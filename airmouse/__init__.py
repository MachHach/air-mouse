from dotenv import load_dotenv

from .server import WebServer


# load environment (does not override)
load_dotenv()


def start_server():
    WebServer().start()
