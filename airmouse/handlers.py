import json

import pyautogui
from tornado.web import RequestHandler
from tornado.websocket import WebSocketHandler


class PageHandler(RequestHandler):
    """
    Handler for a web request
    """
    def get(self):
        screen_size = pyautogui.size()
        self.render("index.html",
            screen={
                'width': screen_size[0],
                'height': screen_size[1],
            }
        )


class SocketHandler(WebSocketHandler):
    """
    Handler for a WebSocket channel
    """
    CONN_LIMIT = 1
    waiters = set()
    
    def open(self):
        limit = SocketHandler.CONN_LIMIT
        waiters = SocketHandler.waiters
        if len(waiters) < limit:
            waiters.add(self)
        else:
            print(f'Reject: Active connections have reached the limit of {limit}')
            self.close()
    
    def on_message(self, message):
        print(f'< {message}')
        dict_obj = json.loads(message)

        try:
            result_pos = self._get_transposed_pos(dict_obj)
            self._perform_gui_action(dict_obj['t'], result_pos[0], result_pos[1])
        except KeyError as e:
            print(f'KeyError: {e}')

    def on_close(self):
        waiters = SocketHandler.waiters
        if self in waiters:
            waiters.remove(self)
    
    def _get_transposed_pos(self, client):
        screen_size = pyautogui.size()
        return (
            client['x'] * screen_size[0] / client['w'],
            client['y'] * screen_size[1] / client['h'],
        )

    def _perform_gui_action(self, mode, x, y):
        if pyautogui.onScreen(x, y):
            if mode == "sclick":
                pyautogui.click(x=x, y=y, duration=0.2)
                print(f'> Click {x:.3f}, {y:.3f}')
            elif mode == "dclick":
                pyautogui.doubleClick(x=x, y=y, duration=0.2)
                print(f'> Double-click {x:.3f}, {y:.3f}')
            elif mode == "rclick":
                pyautogui.rightClick(x=x, y=y, duration=0.2)
                print(f'> Right-click {x:.3f}, {y:.3f}')
            else:
                print(f'Unsupported mode: {mode}')
