# Air Mouse

A web application that enables user to control a machine's GUI.

## Getting started

```bash
pip install -r requirements.txt
python app.py
```

## License

MIT
