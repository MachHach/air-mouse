import airmouse


def main():
    """Main function."""
    airmouse.start_server()


if __name__ == "__main__":
    main()
